from model import Role, User
from acl import AccessControlList

def admin_things():
    print("Do admin things")
    input_key = raw_input()
    print("press 2 to create user\n3 for edit role")
    
    if input_key == "2":
        print("Create User is not supported")
    elif input_key == "3":
        print("Edit role after sometime")
    else:
        print("Wrong choice, logging out")
        return

def guest_things():
    print("Logged in as guest")
    print("press 2 to view roles\n3 for accessing resource")
    input_key = raw_input()
    if input_key == "2":
        print("Create User is not supported")
    elif input_key == "3":
        print("Edit role after sometime")
    else:
        print("Wrong choice, logging out")
        return
    
def interact():
    print('You are logged in as admin')
    while 1:
        print('press 1 for login as guest or 9 to continue or Ctrl-C to exit')
        input_key = raw_input()

        if input_key == "9":
            admin_things()

        elif input_key == "1":
            guest_things()
        
        else:
            print("Wrong choice, please choose from 1 or 9")
        
def createDefaultRoles():
    guest_role = Role('guest')
    admin_role = Role('admin')

    guest_user = User(roles=[guest_role])
    admin_user = User(roles=[admin_role, guest_role])

    acl = AccessControlList()
    acl.resource_read_rule(guest_role, 'GET', '/api/v1/employee/1/info')
    acl.resource_delete_rule(admin_role, 'DELETE', '/api/v1/employee/1/')

    for user_role in [role.get_name() for role in guest_user.get_roles()]:
        assert acl.is_write_allowed(user_role, 'WRITE', '/api/v1/employee/1/info') == False

    for user_role in [role.get_name() for role in guest_user.get_roles()]:
        print(user_role)
        if user_role == 'admin': # as a user can have more than one role assigned to them
            assert acl.is_delete_allowed(user_role, 'DELETE', '/api/v1/employee/1/') == True
        else:
            assert acl.is_delete_allowed(user_role, 'DELETE', '/api/v1/employee/1/') == False
    
    
    interact()

def main():
    print('create default roles')
    createDefaultRoles()

if __name__ == "__main__":
    main()

